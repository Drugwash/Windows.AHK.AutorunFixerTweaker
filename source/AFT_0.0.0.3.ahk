; AFT by Drugwash
; <description>
; <description>
;*****************************************
appname = AFT				; application name
version = 0.0.0.3			; version number
release = January 27, 2010		; release date
type = internal				; release type (internal / public)
iconlocal = AFT.ico			; external icon for uncompiled script
debug = 0					; debug switch (1 = active)
; *****************************************
mylink = drugwash@yahoo.com
ahklink = http://www.autohotkey.com
iconlink = http://www.google.com/search?ie=UTF-8&q=Aqua Neue (Graphite)
abtwin = About %appname%
;************* Custom tray menu *************
if ! notray
	Menu, Tray, Icon, % A_IsCompiled ? "" : iconlocal
Menu, Tray, NoStandard
Menu, Tray, Add, Settings, options
Menu, Tray, Default, Settings
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Exit, getout
;*************** Variables ******************
rk1 = HKCU
rk2 = HKCU
rk3 = HKLM
rk4 = HKLM
rk5 = HKLM
rk6 = HKCU
rk7 = HKCU
rk8 = HKLM
rk9 = HKLM
rk10 = HKCU
rk11 = HKLM
rk12 = HKCU
sk1 = Software\Microsoft\Windows\CurrentVersion\Policies\Explorer
sk2 = Software\Microsoft\Windows\CurrentVersion\Explorer\Desktop\CleanupWiz
sk3 = SYSTEM\CurrentControlSet\Control\CrashControl
sk4 = SYSTEM\CurrentControlSet\Control\CrashControl
sk5 = SOFTWARE\Microsoft\Windows\CurrentVersion\policies\Explorer
sk6 = Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
sk7 = Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
sk8 = SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AlwaysUnloadDLL
sk9 = SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management\PrefetchParameters
sk10 = Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
sk11 = SYSTEM\CurrentControlSet\Control
sk12 = Software\Microsoft\Windows\CurrentVersion\Explorer
vn1 = NoLowDiskSpaceChecks
vn2 = NoRun
vn3 = AutoReboot
vn4 = SendAlert
vn5 = NoDriveTypeAutoRun
vn6 = Hidden
vn7 = ShowSuperHidden
vn8 =
vn9 = EnablePrefetcher
vn10 = DisableThumbnailCache
vn11 = WaitToKillServiceTimeout
vn12 = Link
keys = 12
def = 0
;***************** GUI ********************
Gui, 1:Font, s11 Bold cBlue, Tahoma
Gui, 1:Add, Groupbox, x5 y6 w210 h140 +Center +0x200, Nags
Gui, 1:Add, Groupbox, x5 y146 w210 h150 +Center +0x200, User control
Gui, 1:Add, Groupbox, x220 y6 w210 h140 +Center +0x200, Performance
Gui, 1:Add, Groupbox, x220 y146 w210 h150 +Center 0x200, Appearance
Gui, 1:Font, s7 cDefault Norm, Tahoma
Gui, 1:Add, CheckBox, x10 y32 w200 h14 Check3 vval1 gact, Disable low disk space check
Gui, 1:Add, CheckBox, x10 y47 w200 h14 Check3 vval2 gact, Disable desktop cleanup wizard
Gui, 1:Add, CheckBox, x10 y62 w200 h14 Check3 vval3 gact, Disable autoreboot on crash
Gui, 1:Add, CheckBox, x10 y77 w200 h14 Check3 vval4 gact, Disable alert && error sending on crash
Gui, 1:Add, CheckBox, x10 y92 w200 h14 Check3 vval5 gact, Disable autorun on all drive types
Gui, 1:Add, CheckBox, x10 y172 w200 h14 Check3 vval6 gact, Show hidden files
Gui, 1:Add, CheckBox, x10 y187 w200 h14 Check3 vval7 gact, Show system files
Gui, 1:Add, CheckBox, x225 y32 w200 h14 Check3 vval8 gact, Always unload  unused DLLs
Gui, 1:Add, CheckBox, x225 y47 w200 h14 Check3 vval9 gact, Enable prefetcher
Gui, 1:Add, CheckBox, x225 y62 w200 h14 Check3 vval10 gact, Disable thumbnail cache (thumbs.db)
Gui, 1:Add, CheckBox, x225 y77 w200 h14 Check3 vval11 gact, Short timeout for hung applications
Gui, 1:Add, CheckBox, x225 y172 w200 h14 Check3 vval12 gact, Disable 'Shortcut to' in desktop links
Gui, 1:Add, CheckBox, x225 y92 w200 h14 vval13 Disabled, Disable NTFS Last Access update
Gui, 1:Add, CheckBox, x225 y187 w200 h14 vval14 Disabled, Cascade Control Panel
Gui, 1:Add, CheckBox, x225 y202 w200 h14 vval15 Disabled, Cascade Network Connections
Gui, 1:Add, CheckBox, x225 y217 w200 h14 vval16 Disabled, Show Administrative Tools in Start menu
Gui, 1:Add, CheckBox, x225 y232 w200 h14 vval17 Disabled, Show 'Run' in Start menu
Gui, 1:Add, CheckBox, x225 y247 w200 h14 vval18 Disabled, No personalized menus in Start menu
Gui, 1:Add, CheckBox, x225 y262 w200 h14 vval19 Disabled, Show small icons in Start menu
Gui, 1:Add, CheckBox, x225 y277 w200 h14 vval20 Disabled, Use classic folder appearance
Gui, 1:Add, Text, x5 y355 w105 h20 Center 0x200 Disabled, % "Windows " SubStr(A_OSVersion, 5) " mode"
Gui, 1:Add, Button, x10 y375 w95 h20 gabout, About
Gui, 1:Add, Button, x111 y355 w95 h20 gselall, Select all
Gui, 1:Add, Button, x111 y375 w95 h20 gunselall, Unselect all
Gui, 1:Add, Button, x225 y355 w95 h20 Disabled gapply, Apply
Gui, 1:Add, Button, x225 y375 w95 h20 Disabled gdefault, Default
Gui, 1:Add, Button, x326 y355 w95 h40 ggetout, Exit
Gui, 1:Show, x268 y118 h401 w436, AFT %version% (%type%)
;***************** Read ********************
Loop, %keys%
	{
	RegRead, def%A_Index%, % rk%A_Index%, % sk%A_Index%, % vn%A_Index%
	if ErrorLevel
		{
		def%A_Index% = NONE
		GuiControl,, % "Button" A_Index + 4, -1
		}
	else
		{
		GuiControl, -Check3, Button%A_Index%
		if (def%A_Index% = 0 or def%A_Index% = 1)
			GuiControl,, % "Button" A_Index + 4, % (A_Index = 3 or A_Index = 4 or A_Index = 12) ? !def%A_Index% : def%A_Index%
		else if (A_Index = 11 and def%A_Index% >= 20000)
			GuiControl,, % "Button" A_Index + 4, 0
		else
			 ; assume any positive value as 'enabled'
			GuiControl,, % "Button" A_Index + 4, 1
		}
;	if debug
		IniWrite, % def%A_Index%, values.txt, Defaults, % rk%A_Index% "\" sk%A_Index% " , " vn%A_Index%
	}
Gui, Submit, NoHide
return
;***************** Write ********************
apply:
Loop, %keys%
	{
	IniWrite, % val%A_Index%, values.txt, New, % rk%A_Index% "\" sk%A_Index% " , " vn%A_Index%
	if !debug
		if val%A_Index% != -1
			gosub opt%A_Index%
	}
GuiControl, Disable, Apply
GuiControl, Enable, Default
return
;***************** Default *******************
default:
def = 1
Loop, %keys%
	{
	if def%A_Index% <> NONE
		{
		val%A_Index% := def%A_Index%
		if !debug
			gosub opt%A_Index%
		else
			IniWrite, % val%A_Index%, values.txt, Resets, % rk%A_Index% "\" sk%A_Index% " , " vn%A_Index%
		}
	else
		{
		if !debug
			RegDelete, % rk%A_Index%, % sk%A_Index%, % vn%A_Index%
		else
			IniWrite, % val%A_Index% (Deleted), values.txt, Resets, % rk%A_Index% "\" sk%A_Index% " , " vn%A_Index%
		}
	}
GuiControl, Disable, Default
GuiControl, Disable, Apply
def = 0
return

act:
Gui, Submit, NoHide
GuiControl, Enable, Apply
return

opt1:
RegWrite, REG_DWORD, %rk1%, %sk1%, %vn1%, %val1%
return
opt2:
RegWrite, REG_DWORD, %rk2%, %sk2%, %vn2%, %val2%
return
opt3:
RegWrite, REG_DWORD, %rk3%, %sk3%, %vn3%, % def ? val3 : !val3
return
opt4:
RegWrite, REG_DWORD, %rk4%, %sk4%, %vn4%, % def ? val4 : !val4
return
opt5:
;if !def
	val5 := val5 ? "FF000000" : "00000000"
if debug
	msgbox, %val5%
RegWrite, REG_BINARY, %rk5%, %sk5%, %vn5%, %val5%
return
opt6:
RegWrite, REG_DWORD, %rk6%, %sk6%, %vn6%, %val6%
return
opt7:
RegWrite, REG_DWORD, %rk7%, %sk7%, %vn7%, %val7%
return
opt8:
RegWrite, REG_SZ, %rk8%, %sk8%, %vn8%, %val8%
return
opt9:
if !def
	val9 := val9 ? "3" : "0"
RegWrite, REG_DWORD, %rk9%, %sk9%, %vn9%, %val9%
return
opt10:
RegWrite, REG_DWORD, %rk10%, %sk10%, %vn10%, %val10%
return
opt11:
if !def
	val11 := val11 ? "200" : "20000"
RegWrite, REG_SZ, %rk11%, %sk11%, %vn11%, %val11%
return
opt12:
RegWrite, REG_DWORD, %rk12%, %sk12%, %vn12%, % def ? val12 : !val12
return
opt13:
RegWrite, REG_DWORD, %rk13%, %sk13%, %vn13%, %val13%
return

selall:
Loop, %keys%
	GuiControl,, % "Button" A_Index + 4, 1
goto act

unselall:
Loop, %keys%
	GuiControl,, % "Button" A_Index + 4, 0
goto act

options:
settings:
Reload
return

GuiClose:
getout:
ExitApp

;************** ABOUT box *****************
about:
Gui 1:+Disabled
Gui, 1:Hide
Gui, 2:+owner1 -MinimizeBox
if ! A_IsCompiled
	Gui, 2:Add, Picture, x69 y10 w32 h-1, %iconlocal%
else
	Gui, 2:Add, Picture, x69 y10 w32 h-1 Icon1, %A_ScriptName%
Gui, 2:Font, Bold
Gui, 2:Add, Text, x5 y+10 w160 Center, %appname% v%version%
Gui, 2:Font
Gui, 2:Add, Text, xp y+2 wp Center, by Drugwash
Gui, 2:Add, Text, xp y+2 wp Center gmail0 cBlue, %mylink%
Gui, 2:Add, Text, xp y+10 wp Center, Released %release%
Gui, 2:Add, Text, xp y+2 wp Center, (%type% version)
Gui, 2:Add, Text, xp y+10 wp Center, This product is open-source,
Gui, 2:Add, Text, xp y+2 wp Center, developed in AutoHotkey
Gui, 2:Font,CBlue Underline
Gui, 2:Add, Text, xp y+2 wp Center glink0, %ahklink%
Gui, 2:Font
Gui, 2:Add, Text, xp y+5 wp Center, Icon from the Aqua Neue (Graphite) package at
Gui, 2:Font,CBlue Underline
Gui, 2:Add, Text, xp y+2 wp Center glink1, search Google
Gui, 2:Font
Gui, 2:Add, Button, xp+40 y+20 w80 gdismiss Default, &OK
Gui, 2:Show,, %abtwin%

if ! hCurs
	{
	hCurs := DllCall("LoadCursor", "UInt", NULL, "Int", 32649, "UInt") ;IDC_HAND
	OnMessage(0x200, "WM_MOUSEMOVE")
	}
return

dismiss:
Gui, 1:-Disabled
Gui, 2:Destroy
Gui, 1:Show
IfWinNotExist, %appname%
	{
	OnMessage(0x200,"")
	DllCall("DestroyCursor", "Uint", hCurs)
	}
return

mail0:
mail = mailto:%mylink%
Run, %mail%, UseErrorLevel
return

link0:
Run, %ahklink%,, UseErrorLevel
return

link1:
Run, %iconlink%,, UseErrorLevel
return

WM_MOUSEMOVE(wParam, lParam)
{
Global hCurs, abtwin
WinGetTitle, wind
MouseGetPos,,, winid, ctrl
; change StaticX below to suit the actual window(s)
if wind in %abtwin%
	if ctrl in Static4,Static9,Static11
		DllCall("SetCursor", "UInt", hCurs)
if wind in %appname%
	if ctrl in Static39,Static41,Static43,Static45,Static47,Static49
		DllCall("SetCursor", "UInt", hCurs)
return
}
